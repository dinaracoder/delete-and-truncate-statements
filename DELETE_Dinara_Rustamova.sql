-- Removing rental records related to the film
DELETE FROM rental
WHERE inventory_id IN (
  SELECT inventory_id
  FROM inventory
  WHERE film_id = (SELECT film_id 
                  FROM film 
                  WHERE title = 'Harry Potter and the Philosopher''s Stone'));



-- Removing film inventory records
DELETE FROM inventory
WHERE film_id = (SELECT film_id 
                FROM film 
                WHERE title = 'Harry Potter and the Philosopher''s Stone');

-- Deleting film
-- DELETE FROM film
-- WHERE title = 'Harry Potter and the Philosopher''s Stone';  
-- if needed

-- Deleting customer data
-- Remove any records related to you (as a customer) from all tables except "Customer" and "Inventory"
DELETE FROM payment
WHERE customer_id IN (SELECT customer_id 
                      FROM customer
                      WHERE first_name = 'Dinara');

DELETE FROM rental
WHERE customer_id IN (SELECT customer_id 
                      FROM customer
                      WHERE first_name = 'Dinara');
